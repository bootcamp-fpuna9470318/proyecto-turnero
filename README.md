# Proyecto Turnero (Sistema web para la gestion de turnos) - Bootcamp FPUNA

# Prueba del proyecto
Para hacer una prueba del proyecto podemos clonar el repositorio en la maquina local, para esto debemos tener git instalado en el sistema, para ejecutar el proyecto también debemos tener instalado python en el sistema, para verificar si tenemos instalado ejecutamos **python --version** en una terminal de comandos. Una vez que comprobamos que tenemos instalado python, nos ubicamos en la raiz del proyecto, y creamos un nuevo entorno virtual(opcional), lo activamos y luego instalamos los requerimientos del sistema. Posteriormente nos conectamos con la base de datos, y finalmente iniciamos el servidor con el comando **python manage.py runserver**, accedemos a dirección `http://localhost:8000/` (o cualquier otra dirección que indique la salida del comando de ejecución)


## Clonar repositorio
 
* git clone https://gitlab.com/bootcamp-fpuna9470318/proyecto-turnero.git
*  cd proyecto-turnero



## Creacion y activacion de entorno virtual en Windows

* python -m venv env
* env\Scripts\activate.bat

## Intalacion de los requerimientos 

* pip install -r requirements.txt

# Conexion con la base de datos

## Paso 1

Nos aseguramos de que la base de datos este configurada correctamente en el archivo **turnero/settings.py**. Verificamos los detalles de conexión en la seccion *DATABASE* del archivo

## Paso 2

Realizamos las migraciones: Ejecutamos los siguientes comandos para aplicar las migraciones del proyecto y crear las tablas correspondientes en la base de datos:
* python manage.py makemigrations
* python manage.py migrate

# Iniciar Servidor

-   python manage.py runserver
-   Abrimos la dirección `http://localhost:8000/` (o cualquier otra dirección que indique la salida del comando anterior)

Ahi deberiamos ver el proyecto en funcionamiento

# Logueo en el sistema

El sistema cuenta con dos usuarios predefinidos, un usuario administrador que podrá definir algunos parámetros del sistema, y un usuario Asesor de atención. 

**Usuario Administrador:**
- Usuario: admin
- Contraseña: admin

**Usuario Asesor**
- Usuario: asesor
- Contraseña: asesor

**(Obs): Al loguearse con un usuario administrador podra gestionar los usuarios del sistema, podra Agregar, Editar o Eliminar usuarios**