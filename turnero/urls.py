"""
URL configuration for turnero project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.shortcuts import redirect
from django.urls import include, path

from turnos.views import actualizarCliente, actualizarServicio, actualizarTurno, actualizarUsuario, aggCliente, aggServicio, aggTurno, aggUsuario, comprobar, editarCliente, editarServicio, editarTurno, editarUsuario, eliminarCliente, eliminarServicio, eliminarTurno, eliminarUsuario, guardarCliente, guardarServicio, guardarTurno, guardarUsuario, inicio, lista_cola_espera, listarClientes, listarUsuarios, login, listarServicios, listarPrioridades, listarPermisos, listarRoles

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', lambda request: redirect('login/'), name='root'),
    path('login/',login),
    path('inicio/',inicio),
    path('comprobar/',comprobar),
    path('logout/',login),

    path('colaEspera/',lista_cola_espera),
    path('aggTurno/', aggTurno),
    path('guardarturno/', guardarTurno),
    path('editarturno/<int:id>/',editarTurno),
    path('actualizarturno/',actualizarTurno),
    path('eliminarturno/<int:id>/',eliminarTurno),
    
    path('clientes/',listarClientes),
    path('aggCliente/', aggCliente),
    path('guardarCliente/', guardarCliente),
    path('editarcliente/<int:id>/',editarCliente),
    path('actualizarcliente/',actualizarCliente),
    path('eliminarcliente/<int:id>/',eliminarCliente),
    
    path('servicios/',listarServicios),
    path('aggServicio/', aggServicio),
    path('guardarServicio/', guardarServicio),
    path('editarservicio/<int:id>/',editarServicio),
    path('actualizarservicio/',actualizarServicio),
    path('eliminarservicio/<int:id>/',eliminarServicio),

    path('usuarios/',listarUsuarios),
    path('aggUsuario/', aggUsuario),
    path('guardarUsuario/', guardarUsuario),   
    path('editarusuario/<int:id>/',editarUsuario),
    path('actualizarusuario/',actualizarUsuario),
    path('eliminarusuario/<int:id>/',eliminarUsuario), 

    path('prioridades/',listarPrioridades),
    path('roles/',listarRoles),
    path('permisos/',listarPermisos),

]
