from datetime import datetime
import logging
from django.utils import timezone
from pyexpat.errors import messages
import random
from django.forms import model_to_dict
from django.http import HttpResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_protect
# Create your views here.

from django.template import TemplateDoesNotExist
from django.shortcuts import render

from turnos.models import Cliente, ColaEspera, Estado, Permiso, Prioridad, Rol, Servicios, Usuario

# -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
# Vistas relacionadas al logueo en el sistema y pantalla de inicio
# -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


@csrf_protect
def login(request):
    try:
        cargarDatosPredeterminados()
        return render(request, 'login.html')
    except TemplateDoesNotExist:
        # Manejar la excepción de plantilla que no existe
        return HttpResponse('La plantilla de inicio de sesión no se encontró.')
    except Exception as e:
        # Manejar otras excepciones
        return HttpResponse('Ocurrió un error durante la renderización de la plantilla: {}'.format(str(e)))


@csrf_protect
def comprobar(request):
    try:
        if request.method == 'POST':
            usuario = request.POST['login']
            password = request.POST['password']
            usuario = Usuario.objects.get(nombre=usuario)
            if usuario:
                if usuario.activo == True and usuario.clave == password:
                    request.session['rol'] = usuario.id_rol_fk.rol
                    return redirect('/inicio/')
                if usuario.clave == password  and usuario.activo == False:
                    return render(request, 'login.html', {"error": "Usuario Inactivo"})
                        
    except:
        pass
    return render(request, 'login.html', {"error": "Usuario o contraseña incorrectos"})


from django.shortcuts import render, redirect
from django.contrib.auth.hashers import check_password


@csrf_protect
def inicio(request):
    return render(request, 'inicio.html')

# # # # # # # # # # # # # # # # # # # # # # 
# VISTAS RELACIONADAS A LA COLA DE ESPERA #
# -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
# Vistas para listar, agregar, editar y eliminar turnos en la cola de espera
# -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


@csrf_protect
def lista_cola_espera(request):
    # Obtiene los objetos de la cola de espera ordenados por prioridad y fecha de creación
    cola_espera = ColaEspera.objects.order_by('id_prioridad', 'fecha_creacion')

    # Crea una lista para almacenar los datos a mostrar en la plantilla
    listaDatos = []

    # Itera sobre los objetos de la cola de espera
    for dato in cola_espera:
        # Crear un diccionario para almacenar los datos de cada objeto
        dato_dict = {}
        dato_dict['id_cola_espera'] = dato.id_cola_espera
        dato_dict['numero_ticket'] = dato.numero_ticket
        dato_dict['cliente'] = dato.id_cliente.nombre +" "+ dato.id_cliente.apellido
        dato_dict['estado'] = dato.estado
        dato_dict['prioridad'] = dato.id_prioridad.nivel_nombre
        dato_dict['tipo_servicio'] = dato.id_servicio.nombre_servicio

        # Agrega el diccionario a la lista de datos
        listaDatos.append(dato_dict)

    # Pasa la lista de datos a la plantilla como contexto
    context = {'listaDatos': listaDatos}

    # Renderiza la plantilla con el contexto y devolver la respuesta
    return render(request, 'colaEspera.html', context)


def aggTurno(request):
    try:
        clientes = Cliente.objects.all()
        estados = Estado.objects.all()
        prioridades = Prioridad.objects.all()
        servicios = Servicios.objects.all()
        listaClientes = []
        listaEstados = []
        listaPrioridades = []
        listaServicios = []
        if clientes:
            for c in clientes:
                tieneTurno = ColaEspera.objects.filter(id_cliente=c).exists()
                if not tieneTurno:
                    dic = model_to_dict(c)
                    #print(dic)
                    listaClientes.append(dic)
        if estados:
            for c in estados:
                dic = model_to_dict(c)
                #print(dic)
                listaEstados.append(dic)
        if prioridades:
            for c in prioridades:
                dic = model_to_dict(c)
                #print(dic)
                listaPrioridades.append(dic)

        if servicios:
            for c in servicios:
                dic = model_to_dict(c)
                #print(dic)
                listaServicios.append(dic)

        return render(request, 'aggTurno.html',
                      {"clientes": listaClientes, "estados": listaEstados, "prioridades": listaPrioridades,
                       "servicios": listaServicios})
    except:
        return render(request, 'aggTurno.html',
                      {"clientes": listaClientes, "estados": listaEstados, "prioridades": listaPrioridades,
                       "servicios": listaServicios})


@csrf_protect
def guardarTurno(request):
    if request.method == "POST":
        try:
            colaEspera = ColaEspera()
            colaEspera.id_cliente = Cliente.objects.get(id_cliente=request.POST['cliente'])
            colaEspera.estado = Estado.objects.get(id_estado=request.POST['estado']).estado
            colaEspera.id_prioridad = Prioridad.objects.get(id_prioridad=request.POST['prioridad'])
            colaEspera.id_servicio = Servicios.objects.get(id_tipo_servicio=request.POST['servicio'])
            numero_aleatorio = random.randint(100, 999)
            print(colaEspera.id_cliente)
            print(colaEspera.estado)
            print(colaEspera.id_prioridad)
            print(colaEspera.id_servicio)
            print(numero_aleatorio)
            colaEspera.numero_ticket = str(numero_aleatorio)
            colaEspera.fecha_creacion = timezone.now()
            print(colaEspera.fecha_creacion)
            print(colaEspera)
            colaEspera.save()
        except Exception as e:
            print("Entra en el except")
            print(e)
            pass

    return redirect(lista_cola_espera)


def editarTurno(request, id):
    try:
        colaEspera = ColaEspera.objects.get(id_cola_espera=id)
        estados = Estado.objects.all()
        prioridades = Prioridad.objects.all()
        listaEstados = []
        listaPrioridades = []
        if estados:
            for c in estados:
                dic = model_to_dict(c)
                listaEstados.append(dic)
        if prioridades:
            for c in prioridades:
                dic = model_to_dict(c)
                listaPrioridades.append(dic)
        return render(request, 'editarTurno.html',
                      {"colaEspera": colaEspera, "estados": listaEstados, "prioridades": listaPrioridades})
    except:
        return render(request, 'editarTurno.html',
                      {"colaEspera": colaEspera, "estados": listaEstados, "prioridades": listaPrioridades})


@csrf_protect
def actualizarTurno(request):
    try:
        colaEspera = ColaEspera.objects.get(id_cola_espera=request.POST['id'])
        colaEspera.estado = Estado.objects.get(estado=request.POST['estado']).estado
        colaEspera.id_prioridad = Prioridad.objects.get(id_prioridad=request.POST['prioridad'])
        print(colaEspera)
        colaEspera.save()
    except Exception as e:
        print("entra aca")
        print(e)
        pass
    return redirect(lista_cola_espera)


def eliminarTurno(request, id):
    try:
        turno = ColaEspera.objects.get(id_cola_espera = id)
        turno.delete()
        response = redirect(lista_cola_espera)
        
    except Exception as e:
        response = redirect(lista_cola_espera)
    
    return response


# # # # # # # # # # # # #  # # # # # # 
# VISTAS RELACIONADAS A LOS CLIENTES #
# -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
# Vistas para listar, agregar, editar y eliminar CLIENTES del sistema
# -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


def listarClientes(request):
    try:
        clientes = Cliente.objects.all()
        listaClientes = []
        if clientes:
            for p in clientes:
                dic = model_to_dict(p)
                listaClientes.append(dic)
        return render(request, 'clientes.html', {"clientes": listaClientes})
    except:
        pass
    return render(request, 'clientes.html', {"clientes": listaClientes})


@csrf_protect
def aggCliente(request):
    try:
        sexo = ['M', 'F']
    except:
        pass
    return render(request, 'aggCliente.html', {"sexo": sexo})


@csrf_protect
def guardarCliente(request):
    try:
        cliente = Cliente()
        cliente.CI = request.POST['numeroCI']
        cliente.nombre = request.POST['nombre']
        cliente.apellido = request.POST['apellido']
        cliente.fecha_nacimiento = request.POST['fechanacimiento']
        cliente.telefono = request.POST['telefono']
        cliente.email = request.POST['email']
        cliente.direccion = request.POST['direccion']
        cliente.sexo = request.POST['sexo']
        
        cliente.save()
    except:
        pass
    return redirect(listarClientes)


def editarCliente(request, id):
    try:
        cliente = Cliente.objects.get(id_cliente = id)
        sexo = ['M', 'F']
        return render(request, 'editarCliente.html',
            {"cliente": cliente, "sexo": sexo})
    except:
        pass
    return render(request, 'editarCliente.html',
            {"cliente": cliente, "sexo": sexo})


def actualizarCliente(request):
    try:
        cliente = Cliente.objects.get(id_cliente = request.POST['id'])
        cliente.fecha_nacimiento = request.POST['fechanacimiento']
        cliente.telefono = request.POST['telefono']
        cliente.email = request.POST['email']
        cliente.direccion = request.POST['direccion']
        cliente.sexo = request.POST['sexo']
        print(cliente.fecha_nacimiento+" "+cliente.telefono+ " "+ cliente.email+" "+cliente.direccion+ " "+ cliente.sexo)
        cliente.save()
    except Exception as e:
        print(e)
        pass

    return redirect(listarClientes)


def eliminarCliente(request, id):
    try:
        cliente = Cliente.objects.get(id_cliente = id)
        cliente.delete()
        response = redirect(listarClientes)
        
    except Exception as e:
        response = redirect(listarClientes)
    
    return response


# # # # # # # # # # # # # # # # # # # #
# VISTAS RELACIONADAS A LOS SERVICIOS #
# -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
# Vistas para listar, agregar, editar y eliminar SERVICIOS del sistema
# -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


def listarServicios(request):
    try:
        servicios = Servicios.objects.all()
    except:
        pass
    return render(request, 'servicios.html', {"servicios": servicios})


def aggServicio(request):
    try:
        return render(request, 'aggServicio.html')
    except:
        pass


def guardarServicio(request):
    if request.method == "POST":
        try:
            servicio = Servicios()
            servicio.nombre_servicio = request.POST['nombre']
            servicio.descripcion_servicio = request.POST['descripcion']
            print(servicio.nombre_servicio)
            print(servicio.descripcion_servicio)
            servicio.save()
        except:
            pass

    return redirect(listarServicios)


def editarServicio(request,id):

    try:
        servicio = Servicios.objects.get(id_tipo_servicio=id)
        render (request,'editarServicio.html',{"servicio":servicio})
    except Exception as e:
        print(e)
        pass

    return render (request,'editarServicio.html',{"servicio":servicio})


def actualizarServicio(request):

    try:
        servicio = Servicios.objects.get(id_tipo_servicio = request.POST['id'])
        servicio.nombre_servicio = request.POST['nombreServicio']
        servicio.descripcion_servicio = request.POST['descripcionServicio']

        servicio.save()
    except Exception as e:
        print(e)
        pass

    return redirect(listarServicios)


def eliminarServicio(request, id):
    try:
        servicio = Servicios.objects.get(id_tipo_servicio = id)
        servicio.delete()
        response = redirect(listarServicios)
        
    except Exception as e:
        response = redirect(listarServicios)
    
    return response


# # # # # # # # # # # # # # # # ## # # 
# VISTAS RELACIONADAS A LOS USUARIOS #
# -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
# Vistas para listar, agregar, editar y eliminar USUARIOS del sistema
# -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


def listarUsuarios(request):
    try:
        usuarios = Usuario.objects.all()
        return render(request, 'usuarios.html', {"usuarios": usuarios})
    except:
        pass

def aggUsuario(request):
    try:
        roles = Rol.objects.all()
        listaRoles = []
        
        if roles:
            for r in roles:
                dic = model_to_dict(r)
                print(dic)
                listaRoles.append(dic)

        return render(request, 'aggUsuario.html',
                      {"roles": listaRoles})
    except:
        return render(request, 'aggUsuario.html',
                      {"roles": listaRoles})


@csrf_protect
def guardarUsuario(request):
    if request.method == "POST":
        try:
            usuario = Usuario()
            usuario.nombre = request.POST['nombre']
            usuario.apellido = request.POST['apellido']
            usuario.email = request.POST['email']
            usuario.id_rol_fk = Rol.objects.get(id_rol=request.POST['rol'])
            activo = request.POST.get('activo')
            usuario.clave = request.POST['pass']
            usuario.activo = True if activo == 'on' else False

            print(usuario.nombre)
            print(usuario.apellido)
            print(usuario.email)
            print(usuario.id_rol_fk)
            print(usuario.activo)
            print(usuario)
            usuario.save()
        except Exception as e:
            print("Entra en el except")
            print(e)
            pass

    return redirect(listarUsuarios)



def editarUsuario(request, id):

    try:
        usuario = Usuario.objects.get(id_usuario = id)
        roles = Rol.objects.all()
        listaRoles = []
        
        if roles:
            for r in roles:
                dic = model_to_dict(r)
                print(dic)
                listaRoles.append(dic)

        return render(request, 'editarUsuario.html',
                      {"usuario": usuario, "roles": listaRoles})
    except:
        return render(request, 'editarUsuario.html',
                      {"usuario": usuario, "roles": listaRoles})
    

def actualizarUsuario(request):

    try:
        usuario = Usuario.objects.get(id_usuario = request.POST['id'])
        usuario.email = request.POST['email']
        usuario.id_rol_fk = Rol.objects.get(id_rol = request.POST['rol'])
        usuario.clave = request.POST['pass']
        estado = request.POST.get('activo', False)  # False es el valor predeterminado si no se encuentra el campo
        usuario.activo = estado == 'on'  # Si el valor del checkbox es 'on', establece el campo como True


        usuario.save()

    except Exception as e:
        print(e)
        pass

    return redirect(listarUsuarios)


def eliminarUsuario(request, id):
    try:
        usuario = Usuario.objects.get(id_usuario = id)
        usuario.delete()
        response = redirect(listarUsuarios)
        
    except Exception as e:
        response = redirect(listarUsuarios)
    
    return response


# # # # # # # # # # # # # # # # ## # # # 
# VISTAS RELACIONADAS A LAS PRIORIDADES #
# -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
# Vistas para listar PRIORIDADES, ROLES Y PERMISOS DEL SISTEMA
# -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



def listarPrioridades(request):
    try:
        prioridades = Prioridad.objects.all()
    except:
        pass
    return render(request, 'prioridades.html', {"prioridades": prioridades})


def listarRoles(request):
    try:
        roles = Rol.objects.all()
        return render(request, 'roles.html', {"roles": roles})
    except:
        pass



def listarPermisos(request):
    try:
        permisos = Permiso.objects.all()
        return render(request, 'permisos.html', {"permisos": permisos})
    except:
        pass


def cargarDatosPredeterminados():
    

    if not Permiso.objects.exists():
        permisos2 = Permiso(
            codigo="P2",
            descripcion="Agregar | Modificar (Clientes, Turnos)"
        )
        permisos2.save()
    
        permisos1 = Permiso(
                codigo="P1",
                descripcion="Agregar | Modificar | Eliminar (Usuarios, Servicios, Clientes, Turnos)	"
            )
        permisos1.save()


    if not Rol.objects.exists():
        rol1 = Rol(
                rol="Admin",
                descripcion="Tiene todos los permisos",
                id_permiso_fk= permisos1
            )
        rol1.save()

        rol2 = Rol(
            rol="Asesor",
            descripcion="Tiene funcionalidades limitadas dentro del sistema",
            id_permiso_fk=permisos2  
        )
        rol2.save()

    if not Usuario.objects.exists():
        user1 = Usuario(
            nombre="admin",
            apellido="admin",
            email="Admin@gmail.com",
            id_rol_fk=rol1,  
            clave="admin",
            activo=True
        )
        user1.save()

        user2 = Usuario(
            nombre="asesor",
            apellido="asesor",
            email="asesor@gmail.com",
            id_rol_fk=rol2,  
            clave="asesor",
            activo=True
        )
        user2.save()

    if not Prioridad.objects.exists():
        prioridad1 = Prioridad(
            nivel_de_prioridad = 3,
            nivel_nombre = "ALTA",
            descripcion_prioridad = "3ra Edad, Embarazadas, Discapacidad"
        )
        prioridad1.save()

        prioridad2 = Prioridad(
            nivel_de_prioridad = 3,
            nivel_nombre = "MEDIA",
            descripcion_prioridad = "Clientes corporativos"
        )
        prioridad2.save()

        prioridad3 = Prioridad(
            nivel_de_prioridad = 3,
            nivel_nombre = "BAJA",
            descripcion_prioridad = "Clientes personales"
        )
        prioridad3.save()

        
