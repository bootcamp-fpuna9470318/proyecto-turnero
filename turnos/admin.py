from django.contrib import admin

from turnos.models import Cliente, ColaEspera, Estado, Permiso, Prioridad, Puesto, Rol, Servicios, Usuario

# Register your models here.



admin.site.register(Cliente)
admin.site.register(Puesto)
admin.site.register(Permiso)
admin.site.register(Usuario)
admin.site.register(Servicios)
admin.site.register(Prioridad)
admin.site.register(ColaEspera)
admin.site.register(Estado)
admin.site.register(Rol)

