from datetime import timezone
from django.db import models

SEXO_CHOICES = [
    ('M','M'),
    ('F','F'),
]

class Cliente(models.Model):
    id_cliente = models.AutoField(primary_key=True)
    CI = models.CharField(unique=True, max_length=16)
    nombre = models.CharField(max_length=64,default="")
    apellido =  models.CharField(max_length=64,default="")
    fecha_nacimiento = models.DateField(null=True)
    telefono = models.CharField(max_length=20, blank=True, null=True)
    email = models.CharField(max_length=50, blank=True, null=True)
    direccion = models.CharField(max_length=70, blank=True, null=True)
    sexo = models.CharField(max_length=1, choices=SEXO_CHOICES,default="")

    class Meta:
        db_table = 'cliente'

ESTADOS_CHOICES = [
    ('PENDIENTE','PENDIENTE'),
    ('EN CURSO','EN CURSO'),
    ('FINALIZADO','FINALIZADO'),
    ('CANCELADO','CANCELADO'),
]
class ColaEspera(models.Model):
    id_cola_espera = models.AutoField(primary_key=True)
    id_cliente = models.ForeignKey(Cliente, on_delete=models.CASCADE, db_column='id_cliente')
    estado = models.CharField(max_length=15, choices=ESTADOS_CHOICES,default="")
    id_prioridad = models.ForeignKey('Prioridad', on_delete=models.CASCADE, db_column='id_prioridad')
    id_servicio = models.ForeignKey('Servicios', on_delete=models.CASCADE, db_column='id_tipo_servicio')
    numero_ticket = models.CharField(max_length=10)
    fecha_creacion = models.DateTimeField(null=True) 

    class Meta:
        db_table = 'cola_espera'


class Estado(models.Model):
    id_estado = models.AutoField(primary_key=True)
    estado = models.CharField(max_length=15, choices=ESTADOS_CHOICES)

    class Meta:
        db_table = 'estado'


class Permiso(models.Model):
    id_permiso = models.SmallAutoField(primary_key=True)
    codigo = models.CharField(max_length=8)
    descripcion = models.CharField(max_length=100)

    class Meta:
        db_table = 'permiso'

PRIORIDAD_CHOICES = [
    (1,'BAJA'),
    (2,'MEDIA'),
    (3,'ALTA'),
]
class Prioridad(models.Model):
    id_prioridad = models.AutoField(primary_key=True)
    nivel_de_prioridad = models.IntegerField(choices=PRIORIDAD_CHOICES)
    nivel_nombre = models.CharField(max_length=50,blank=True, null=True)
    descripcion_prioridad = models.CharField(max_length=50,blank=True, null=True)

    class Meta:
        db_table = 'prioridad'


class Puesto(models.Model):
    id_puesto = models.AutoField(primary_key=True)
    nombre_puesto = models.CharField(max_length=20)
    id_tipo_servicio_fk = models.ForeignKey('Servicios', on_delete=models.CASCADE, db_column='id_tipo_servicio_fk')

    class Meta:
        db_table = 'puesto'


class Rol(models.Model):
    id_rol = models.SmallAutoField(primary_key=True)
    rol = models.CharField(max_length=8)
    descripcion = models.CharField(max_length=100)
    id_permiso_fk = models.ForeignKey(Permiso, on_delete=models.CASCADE, db_column='id_permiso_fk')
    

    class Meta:
        db_table = 'rol'


class Servicios(models.Model):
    id_tipo_servicio = models.AutoField(primary_key=True)
    nombre_servicio = models.CharField(max_length=20)
    descripcion_servicio = models.CharField(max_length=60)

    class Meta:
        db_table = 'servicios'


class Usuario(models.Model):
    id_usuario = models.SmallAutoField(primary_key=True)
    nombre = models.CharField(max_length=20)
    apellido =  models.CharField(max_length=20,default="")
    email = models.CharField(max_length=50, blank=True, null=True)
    id_rol_fk = models.ForeignKey(Rol, on_delete=models.CASCADE, db_column='id_rol_fk')
    clave = models.CharField(max_length=32)
    activo = models.BooleanField()

    class Meta:
        db_table = 'usuario'

